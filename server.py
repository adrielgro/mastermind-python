#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 25 15:54:56 2018

@author: groxor
Ref: https://shakeelosmani.wordpress.com/2015/04/13/python-3-socket-programming-example/
"""

import random
import socket
import pickle
import _thread

ganadores = []

def GenerarCodigoSecreto(colors):
    
    random.shuffle(colors)
    return colors
 
def Main():
    def new_client(clientsocket, addr):
        print("Se ha conectado un jugador: " + str(addr))  # Imprimimos la conexion, debug
        while True:
            data_conn = clientsocket.recv(1024)  # Decodificamos la lista recibida del cliente
            if not data_conn: # Si recibimos paquetes sin datos salimos
                print("Se ha desconectado el cliente: " + str(addr))
                break

            data = pickle.loads(data_conn)

            print ("Datos recibidos de: " + str(data)) # debug

            # Evaluamos los datos recibidos y los procesamos
            validation = [0,0, None]
            for x in range(9):
                if data[x] == codigoSecreto[x]:
                    validation[0] += 1 # Posicion correcta
                else:
                    validation[1] += 1 # Posicion incorrecta

            if validation[0] == 9: # Si este jugador tiene todas las respuestas correctas..
                ganadores.append(str(data[9])) #  metemos su nickname a la lista de ganadores

            validation[2] = ganadores # Adjuntamos la lista de ganadore

            data = pickle.dumps(validation) # Serializamos los datos

            # Le contestamos al jugador
            print ("Datos enviados: " + str(validation)) # debug
            clientsocket.send(data) # Le contestamos al cliente con las validaciones

        clientsocket.close() # Cerramos la conexion

    # Datos de los socket
    host = "127.0.0.1"
    port = 5003
    
    # Datos del juego
    colors = ['azul', 'amarillo', 'anaranjado', 'blanco', 'gris', 'morado', 'negro', 'rojo', 'rosa']
    
    # Generamos el codigo secreto
    codigoSecreto = [] # Inicializamos una lista vacio
    codigoSecreto = GenerarCodigoSecreto(colors) # Obtenemos el codigo aleatorio
    
    print("Codigo secreto: " + str(codigoSecreto)) # debug
     
    s = socket.socket() # Instanciamos la clase socket
    s.bind((host,port)) # Bindeamos la IP y puerto
     
    s.listen(1) # Ponemos la conexion a la escucha

    while True:
        try:
            c, addr = s.accept()  # Establecemos la conexion con el cliente obteniendo la direccion
            _thread.start_new_thread(new_client, (c, addr))
        except KeyboardInterrupt:
            print("Conexion cerrada")
            break
    s.close()


     
if __name__ == '__main__':
    Main()