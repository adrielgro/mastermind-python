#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 25 16:05:47 2018

@author: groxor
"""

import socket
import pickle
import collections
import sys

from tkinter import *  # Carga módulo tk (widgets estándar)
from tkinter import ttk  # Carga ttk (para widgets nuevos 8.5+)
from tkinter import messagebox


def Main():
    if len(sys.argv) > 1:
        name_player = sys.argv[1]
    else:
        print("Necesitas introducir tu nickname, use: python3 client.py SuNickname")
        return

    host = '127.0.0.1'  # Direccion IP del servidor
    port = 5003  # Puerto del servidor
    game = 1  # 0=Terminado, 1=Iniciado,
    max_intents = 3
    cur_intents = 0

    try:
        mySocket = socket.socket()  # Instanciamos la clase socket
        mySocket.connect((host, port))  # Bindeamos la IP y puerto
    except ConnectionRefusedError:
        print("No se ha podido establecer la conexion con el servidor.")
        return

    while game == 1:  # Mientras el mensaje no sea 'q'
        cur_intents = cur_intents + 1
        message = [None] * 10

        def SendData(*colores):
            for x in range(9):
                message[x] = colores[x].get() # Adjuntamos las respuestas
            message[9] = name_player # Adjuntamos el nickname del jugador

            for x, y in collections.Counter(message).items():
                if y > 1:
                    print("Valor repetido: " + str(x))
                    winmsg = Tk()
                    winmsg.withdraw()
                    msgbox = messagebox.showinfo("Servidor", "Tienes respuestas repetidas, verifica que sean unicas.", parent=winmsg)
                    if msgbox:
                        winmsg.destroy()
                    return

            mySocket.send(pickle.dumps(message))  # Codificamos el mensaje y lo enviamos al servidor
            gui.destroy()

        # for x in range(9):
        #    message[x] = input(" -> ") # Obtener las respuestas y guardarlas en la variable message

        # Inicializamos la ventana con sus respectivos valores
        gui = Tk()
        gui.geometry('600x300')
        gui.title('MasterMind')

        # Definimos los componentes
        # ttk.Label(gui, text='Colores:').pack(side='left')

        # GUI
        color1 = StringVar(gui)
        color2 = StringVar(gui)
        color3 = StringVar(gui)
        color4 = StringVar(gui)
        color5 = StringVar(gui)
        color6 = StringVar(gui)
        color7 = StringVar(gui)
        color8 = StringVar(gui)
        color9 = StringVar(gui)

        color1.set("Ninguno")
        color2.set("Ninguno")
        color3.set("Ninguno")
        color4.set("Ninguno")
        color5.set("Ninguno")
        color6.set("Ninguno")
        color7.set("Ninguno")
        color8.set("Ninguno")
        color9.set("Ninguno")
        w = Label(gui, text="MASTERMIND!")
        w.pack()
        w.config(font=("Courier", 20))
        w2 = Label(gui, text="Encuentra el codigo secreto, antes que los demas.")
        w2.pack()
        w2.config(font=("Courier", 14))
        ttk.OptionMenu(gui, color1, "Ninguno", 'azul', 'amarillo', 'anaranjado', 'blanco', 'gris', 'morado', 'negro',
                       'rojo', 'rosa').place(x=120, y=120)
        ttk.OptionMenu(gui, color2, "Ninguno", 'azul', 'amarillo', 'anaranjado', 'blanco', 'gris', 'morado', 'negro',
                       'rojo', 'rosa').place(x=120, y=150)
        ttk.OptionMenu(gui, color3, "Ninguno", 'azul', 'amarillo', 'anaranjado', 'blanco', 'gris', 'morado', 'negro',
                       'rojo', 'rosa').place(x=120, y=180)
        ttk.OptionMenu(gui, color4, "Ninguno", 'azul', 'amarillo', 'anaranjado', 'blanco', 'gris', 'morado', 'negro',
                       'rojo', 'rosa').place(x=250, y=120)
        ttk.OptionMenu(gui, color5, "Ninguno", 'azul', 'amarillo', 'anaranjado', 'blanco', 'gris', 'morado', 'negro',
                       'rojo', 'rosa').place(x=250, y=150)
        ttk.OptionMenu(gui, color6, "Ninguno", 'azul', 'amarillo', 'anaranjado', 'blanco', 'gris', 'morado', 'negro',
                       'rojo', 'rosa').place(x=250, y=180)
        ttk.OptionMenu(gui, color7, "Ninguno", 'azul', 'amarillo', 'anaranjado', 'blanco', 'gris', 'morado', 'negro',
                       'rojo', 'rosa').place(x=380, y=120)
        ttk.OptionMenu(gui, color8, "Ninguno", 'azul', 'amarillo', 'anaranjado', 'blanco', 'gris', 'morado', 'negro',
                       'rojo', 'rosa').place(x=380, y=150)
        ttk.OptionMenu(gui, color9, "Ninguno", 'azul', 'amarillo', 'anaranjado', 'blanco', 'gris', 'morado', 'negro',
                       'rojo', 'rosa').place(x=380, y=180)

        ttk.Button(gui, text='Enviar',
                   command=lambda: [SendData(color1, color2, color3, color4, color5, color6, color7, color8,
                                            color9)]).pack(side=BOTTOM)

        gui.mainloop()

        try:
            # Esperamos la respuesta del servidor
            data_conn = mySocket.recv(1024)
            if not data_conn:  # Si recibimos paquetes sin datos salimos
                print("Conexion cerrada")
                break
            data = pickle.loads(data_conn)  # Recibimos el mensaje y lo decodificamos
        except KeyboardInterrupt:
            print("Conexion cerrada")
            break
        print('Respuestas correctas: ' + str(data[0]))
        print('Respuestas incorrectas: ' + str(data[1]))
        #print ('Servidor: ' + str(data)) # Imprimimos el mensaje, debug
        print('Lista de ganadores en orden:' + str(data[2]))

        # message = input(" -> ") # Volvemos a enviarle una respuesta al servidor
        if data[0] == 9:
            winmsg = Tk()
            winmsg.withdraw()
            messagebox.showinfo("Servidor", "¡Felicidades, has ganado! Acertaste todas las respuestas :)")
            msgbox = messagebox.showinfo("Servidor", "Lista de ganadores en orden: " + str(data[2]))
            if msgbox:
                winmsg.destroy()
            game = 0
        else:
            if max_intents == cur_intents:
                winmsg = Tk()
                winmsg.withdraw()
                messagebox.showinfo("Servidor", "Has utilizado los " + str(max_intents) + " intentos permitidos, bye :(")
                msgbox = messagebox.showinfo("Servidor", "Lista de ganadores en orden: " + str(data[2]))
                if msgbox:
                    winmsg.destroy()
                game = 0
            else:
                winmsg = Tk()
                winmsg.withdraw()
                messagebox.showinfo("Servidor", "Tienes " + str(data[0]) + " respuetas correctas y " + str(data[1]) + " respuestas incorrectas", parent=winmsg)
                msgbox = messagebox.showinfo("Servidor", "Por el momento han ganado: " + str(data[2]), parent=winmsg)
                if msgbox:
                    winmsg.destroy()

    mySocket.close()  # Cerramos la conexion


if __name__ == '__main__':
    Main()